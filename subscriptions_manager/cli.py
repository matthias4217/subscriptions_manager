from argparse import ArgumentParser
import logging

from subscriptions_manager.app import App

def main() -> int:
    logging.basicConfig(encoding="utf-8", level=logging.WARN)
    return real_main()

def real_main() -> int:
    parser = ArgumentParser(
        prog="Subscription manager",
        description="Manage your subscriptions more easily",
    )
    subparsers = parser.add_subparsers(help="sub-command help")
    # Subcommand to list subscriptions
    parser_list_subscriptions = subparsers.add_parser(
        "list-subscriptions", help="list-subscriptions", aliases=["ls"]
    )
    parser_list_subscriptions.add_argument("-s", "--summary", action='store_true', default=False)
    parser_list_subscriptions.set_defaults(func=list_subscriptions)

    args = parser.parse_args()
    print(args)
    return args.func(args)


def list_subscriptions(args):
    """
    :param args Namespace
    """
    app = App()
    app.list_creator_pages(summarize=args.summary)
    return 0


if __name__ == "__main__":
    raise SystemExit(real_main(sys.argv))
