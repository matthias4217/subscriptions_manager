import logging

import yaml

from subscriptions_manager.patreon import PatreonManager

class App:

    def __init__(self, config="config.yaml"):
        with open(config, "r") as f:
            self.config = yaml.safe_load(f)
        self.patreon_manager = PatreonManager()
    
    def list_creator_pages(self, summarize: bool=False):
        for creator_page_id, creator_page_values in self.config["creator_pages"].items():
            if creator_page_values["platform"] == "patreon":
                creator_id = creator_page_values["url"].strip("/").split("/")[-1]
                (tiers,) = self.patreon_manager.get_creator(creator_url=creator_page_values["url"])
                if creator_page_values["subscribed"]:
                    subscription = f" [{creator_page_values['current_tier']}]"
                else:
                    subscription = ""
                if not(summarize):
                    print(f" {creator_page_id}{subscription} : {' - '.join(tiers)}")
        if summarize:
            totals = {}
            for _,creator_page_values in self.config["creator_pages"].items():
                if not creator_page_values["subscribed"]:
                    continue
                if not creator_page_values["platform"] in totals:
                    totals[creator_page_values["platform"]] = 0
                totals[creator_page_values["platform"]] += creator_page_values["current_tier"]
            print(totals)