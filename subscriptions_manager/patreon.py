import logging
import requests

from bs4 import BeautifulSoup

class PatreonManager:

    def __init__(self):
        pass

    def get_creator(self, creator_url: str=None, creator_id: str=None) -> tuple:
        creator_page_html = requests.get(creator_url).text
        soup = BeautifulSoup(creator_page_html, 'html.parser')
        tiers = soup.find_all(class_="ewXkmn")
        creator_tiers = []
        for tier in tiers:
            creator_tiers.append(tier.text.split()[0])
        return (creator_tiers,)
