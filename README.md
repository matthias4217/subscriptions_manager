# Subscription manager

🚧 This is still a work in progress.
There are already some basic features, but there will be some overhauls in the future.
In particular, the structure of the config file may change, as well as the CLI options and output. 🚧

This project aims at offering a CLI tool to better manage the subscription you’ve made to creators.
It hopes to support Patreon, Ko-Fi and more.

## Supported platforms

Currently, this tool only supports [Patreon](https://www.patreon.com/).
In the future, it should also support the following :
* [Liberapay](https://liberapay.com/)
* [Ko-Fi](https://ko-fi.com/)
* [Tipeee](https://www.tipeee.com/)

## Configuration

The software requires the creation of a `config.yaml` file.

### Format

This must be a valid yaml file.
There must be a `creator_pages`, containing a dictionary of creators.
The latters can have the following attributes :

| Name           | Type      | Required | Description                                        |
| -------------- | --------- | -------- | -------------------------------------------------- |
| `url`          | `string`  | **Yes**  | The URL to the creator page                        |
| `platform`     | `string`  | **Yes**  | `patreon`, `liberapay`, `kofi`                     |
| `subscribed`   | `boolean` | **Yes**  | Whether you’re subscribed to the creator.          |
| `current_tier` | `float`   | *        | The amount paid at your current tier               |
| `type`         | `string`  | *        | `daily`, `weekly`, `monthly`, `yearly`, `itemized` |
| `description`  | `string`  | No       | A description of the creator page                  |

### Example configuration

```yaml
creator_pages:
  foo:
    url: https://ko-fi.com/foo/
    platform: kofi
    subscribed: true
    current_tier: 1
    type: monthly
  bar:
    url: https://liberapay.com/bar/
    platform: liberapay
    subscribed: false
  foo:
    url: https://patreon.com/baz/
    platform: patreon
    subscribed: true
    current_tier: 2.5
    type: weekly
```

## Usage

### List subscriptions

The following command will output a list of the creators from supported platforms, with their identifier followed by the current tier you’re at, and a list of their available tiers.

```sh
$ subscriptions_manager ls
 baz [2.5] : €2 - €5 - €10
$ subscriptions_manager ls -s # the -s option displays only the total amount of your current subscriptions
 {'kofi': 1.2, 'liberapay': 2.3, 'patreon': 4.5}
```
